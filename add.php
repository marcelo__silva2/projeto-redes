<html>
    <head>
        <title>Inserir Dados</title>
    </head>

    <body>
        <?php
            //incluir a conexao
            require_once('config.php');
            if(isset($_POST['Submit'])){
                /*Esta função escapa caracteres especiais na string para usar em SQL.
                Caracteres codificados sao NULL mais \n, \n, \, ', ", e Control-Z */
                $name = mysqli_real_escape_string($mysqli, $_POST['name']);
                $age = mysqli_real_escape_string($mysqli, $_POST['age']);
                $email = mysqli_real_escape_string($mysqli, $_POST['email']);
                //verificar campos vazios
                if(empty($name) || empty($age) || empty($email)){
                    if(empty($name)){
                        echo "<font color='red'>Nome não preenchido.</font><br />";
                    }
                    if(empty($age)){
                        echo "<font color='red'>Idade não preenchido.</font><br />";
                    }
                    if(empty($email)){
                        echo "<font color='red'>Email não preenchido.</font><br />";
                    }
                    //link para a pagina previa
                    echo "<br /><a href='javascript:self.history.back();'>Voltar</a>";
                }
                else {
                    $result= mysqli_query($mysqli, "INSERT INTO users(name,age,email) VALUES('$name','$age','$email')");
                    echo "<font color='green'>Dados acrescentados com sucesso.</font>";
                    echo "<br /><a href='index.php'>Ver Resultados</a>";
                }
            }
         ?>
    </body>
</html>
